<?php

namespace common\models;

use Yii;
use yii\base\NotSupportedException;
use yii\behaviors\TimestampBehavior;
use yii\db\ActiveRecord;
use yii\web\IdentityInterface;

class User extends \Da\User\Model\User
{

    public function getPosts()
    {
        return $this
            ->hasMany(Post::class, ['id' => 'post_id'])
            ->viaTable('user_post', ['user_id' => 'id']);
    }
}
