<?php

namespace common\models;

use Yii;

/**
 * Model class for Profile.
 *
 * @property string $profile_picture
 * @property string $phone
 * @property string $mobile
 * @property string $is_provider
 */
class Profile extends \Da\User\Model\Profile
{
    public function rules()
    {
        return array_merge(parent::rules(), [
            [['profile_picture', 'phone', 'mobile'], 'string'],
            [['is_provider'], 'boolean'],
        ]);
    }

    public function attributeLabels()
    {
        return array_merge(parent::attributeLabels(), [
            'profile_picture' => Yii::t('app', 'Profile Picture'),
            'phone' => Yii::t('app', 'Phone'),
            'mobile' => Yii::t('app', 'Mobile'),
            'is_provider' => Yii::t('app', 'Is Provider'),
        ]);
    }
}
