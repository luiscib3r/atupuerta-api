<?php

namespace common\models;

/**
 * This is the model class for table "{{%post}}".
 *
 * @property int         $id
 * @property string      $title
 * @property null|string $image1
 * @property null|string $image2
 * @property null|string $image3
 * @property null|float  $price
 * @property null|string $moneyType
 * @property string      $description
 * @property string      $category
 * @property bool        $active
 * @property Comment[]   $comments
 * @property User        $createdBy
 */
class Post extends ActiveRecord
{
    /**
     * {@inheritdoc}
     */
    public static function tableName()
    {
        return '{{%post}}';
    }

    /**
     * {@inheritdoc}
     */
    public function rules()
    {
        return [
            [['title', 'description'], 'required'],
            [['price'], 'number'],
            [['active'], 'boolean'],
            [['description'], 'string'],
            [['created_by'], 'integer'],
            [['title', 'moneyType', 'category'], 'string', 'max' => 255],
            [['created_by'], 'exist', 'skipOnError' => true, 'targetClass' => User::className(), 'targetAttribute' => ['created_by' => 'id']],
        ];
    }

    /**
     * {@inheritdoc}
     */
    public function attributeLabels()
    {
        return [
            'id' => 'ID',
            'title' => 'Title',
            'image1' => 'Image Url 1',
            'image2' => 'Image Url 2',
            'image3' => 'Image Url 3',
            'price' => 'Price',
            'moneyType' => 'Money Type',
            'description' => 'Description',
            'category' => 'Category',
        ];
    }

    public function getUsers()
    {
        return $this
            ->hasMany(User::class, ['id' => 'user_id'])
            ->viaTable('user_post', ['post_id' => 'id'])
        ;
    }

    /**
     * {@inheritdoc}
     *
     * @return \common\models\query\PostQuery the active query used by this AR class
     */
    public static function find()
    {
        return new \common\models\query\PostQuery(get_called_class());
    }
}
