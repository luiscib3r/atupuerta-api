<?php

namespace console\migrations;

use common\models\User;
use Yii;


/**
 * Class m200502_050121_add_admin_users
 */
class m200502_050121_add_admin_users extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $user = new User([
            'scenario' => 'create',
            'email' => 'info@atupuerta.com',
            'username' => 'atupuerta',
            'password' => '1234567',
            'confirmed_at' => time(),
        ]);
        $user->save();

        $auth = Yii::$app->getAuthManager();
        $userRole = $auth->createRole('admin');
        $auth->add($userRole);
        $auth->assign($userRole, $user->id);
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        User::deleteAll();
    }

    /*
    // Use up()/down() to run migration code without a transaction.
    public function up()
    {

    }

    public function down()
    {
        echo "m200502_050121_add_admin_users cannot be reverted.\n";

        return false;
    }
    */
}
