<?php

namespace console\migrations;

/**
 * Handles the creation of table `{{%feedback}}`.
 * Has foreign keys to the tables:.
 *
 * - `{{%user}}`
 */
class m200504_001348_create_feedback_table extends BaseMigration
{
    /**
     * {@inheritdoc}
     */
    public function safeUp()
    {
        $this->createTable('{{%feedback}}', [
            'id' => $this->primaryKey(),
            'user_id' => $this->integer()->notNull(),
            'text' => $this->string(),
        ]);

        // creates index for column `user_id`
        $this->createIndex(
            '{{%idx-feedback-user_id}}',
            '{{%feedback}}',
            'user_id'
        );

        // add foreign key for table `{{%user}}`
        $this->addForeignKey(
            '{{%fk-feedback-user_id}}',
            '{{%feedback}}',
            'user_id',
            '{{%user}}',
            'id',
            'CASCADE'
        );
    }

    /**
     * {@inheritdoc}
     */
    public function safeDown()
    {
        // drops foreign key for table `{{%user}}`
        $this->dropForeignKey(
            '{{%fk-feedback-user_id}}',
            '{{%feedback}}'
        );

        // drops index for column `user_id`
        $this->dropIndex(
            '{{%idx-feedback-user_id}}',
            '{{%feedback}}'
        );

        $this->dropTable('{{%feedback}}');
    }
}
