<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\User;
use common\models\Feedback;
use Yii;

class FeedbackController extends \api\common\controllers\RestController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => \sizeg\jwt\JwtHttpBearerAuth::class,
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actionIndex()
    {
        $userId = isset(Yii::$app->user->id)
        ? Yii::$app->user->id
        : Yii::$app->request->get('userId');
        $user = User::findOne($userId);

        if (!$user) {
            return [
                'message' => 'Debe estar registrado para realizar esta acción',
            ];
        }

        $text = Yii::$app->getRequest()->post('text');

        $feedback = new Feedback(['text' => $text]);

        $feedback->link('user', $user);

        $feedback->save();

        return [
            'message' => 'ok',
        ];
    }

    public function verbs()
    {
        $verbs = parent::verbs();
        $verbs['index'] = ['post', 'options'];

        return $verbs;
    }
}
