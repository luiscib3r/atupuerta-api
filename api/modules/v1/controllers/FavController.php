<?php

namespace api\modules\v1\controllers;

use api\common\controllers\RestController;
use api\modules\v1\models\Food;
use api\modules\v1\models\User;
use Yii;

class FavController extends RestController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => \sizeg\jwt\JwtHttpBearerAuth::class,
            'optional' => [
                'create',
            ],
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actionCreate()
    {
        $user = User::findOne(Yii::$app->user->id);
        $food = Food::findOne(Yii::$app->request->get('postId'));

        $message = '';
        if (!$user || !$food) {
            $message = 'error';
        } elseif ($user->getPosts()->where(['id' => $food->id])->exists()) {
            $user->unlink('posts', $food, true);
            $message = 'unsaved';
        } else {
            $user->link('posts', $food);
            $message = 'saved';
        }

        return [
            'message' => $message,
        ];
    }

    public function actionIndex()
    {
        $userId = isset(Yii::$app->user->id)
            ? Yii::$app->user->id
            : Yii::$app->request->get('userId');
        $user = User::findOne($userId);

        if (!$user) {
            return [
                'message' => 'error',
            ];
        }

        return $user->getFoods()
            ->andWhere(['active' => true])
            ->all()
        ;
    }

    public function verbs()
    {
        $verbs = parent::verbs();
        $verbs['create'] = ['post', 'options'];
        $verbs['index'] = ['get', 'options'];

        return $verbs;
    }
}
