<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\Comment;
use Yii;
use yii\data\ActiveDataProvider;

class CommentController extends \api\common\controllers\ActiveController
{
    public $modelClass = Comment::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => \sizeg\jwt\JwtHttpBearerAuth::class,
            'optional' => [
                'index', 'view',
            ],
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function actions()
    {
        $actions = parent::actions();

        $actions['index']['prepareDataProvider'] = [$this, 'prepareDataProvider'];

        return $actions;
    }

    public function prepareDataProvider()
    {
        $postId = Yii::$app->request->get('postId');

        if (isset($postId)) {
            return new ActiveDataProvider([
                'query' => $this->modelClass::find()->andWhere(['post_id' => $postId]),
                'pagination' => [
                    'class' => 'yii\data\Pagination',
                    'validatePage' => false,
                ],
            ]);
        }

        return new ActiveDataProvider([
            'query' => $this->modelClass::find(),
            'pagination' => [
                'class' => 'yii\data\Pagination',
                'validatePage' => false,
            ],
        ]);
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        if ('update' === $action || 'delete' === $action) {
            if ($model->created_by !== \Yii::$app->user->id) {
                throw new \yii\web\ForbiddenHttpException(sprintf('No tienes permiso para realizar esta acción'));
            }
        }
    }
}
