<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\User;

class UserController extends \api\common\controllers\ActiveController
{
    public $modelClass = User::class;

    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => \sizeg\jwt\JwtHttpBearerAuth::class,
            'optional' => [
                'create',
            ],
            'except' => ['options'],
        ];

        return $behaviors;
    }

    public function checkAccess($action, $model = null, $params = [])
    {
        // check if the user can access $action and $model
        // throw ForbiddenHttpException if access should be denied
        if ('update' === $action || 'delete' === $action) {
            if ($model->id !== \Yii::$app->user->id) {
                throw new \yii\web\ForbiddenHttpException(sprintf('No tienes permiso para realizar esta acción'));
            }
        }
    }
}
