<?php

namespace api\modules\v1\controllers;

use api\modules\v1\models\User;
use Yii;

/**
 * Password controller for the `v1` module.
 */
class PasswordController extends \api\common\controllers\RestController
{
    public function behaviors()
    {
        $behaviors = parent::behaviors();

        $behaviors['authenticator'] = [
            'class' => \sizeg\jwt\JwtHttpBearerAuth::class,
            'except' => ['options'],
        ];

        return $behaviors;
    }

    /**
     * Password action.
     */
    public function actionIndex()
    {
        $body = Yii::$app->getRequest()->getBodyParams();

        Yii::$app->response->statusCode = 422;

        if (!isset($body['old_password'])) {
            return $this->asJson([
                [
                    'field' => 'old_password',
                    'message' => 'Actual password cannot be blank.',
                ],
            ]);
        }

        if (!isset($body['new_password'])) {
            return $this->asJson([
                [
                    'field' => 'new_password',
                    'message' => 'New password cannot be blank.',
                ],
            ]);
        }

        $uid = Yii::$app->user->id;

        if (!isset($uid)) {
            Yii::$app->response->statusCode = 401;

            return [
                'name' => 'Unauthorized',
                'message' => 'Your request was made with invalid credentials.',
                'code' => 0,
                'status' => 401,
            ];
        }

        $user = User::findOne($uid);

        if (!$user || !$user->validatePassword($body['old_password'])) {
            Yii::$app->response->statusCode = 401;

            return [
                'name' => 'Unauthorized',
                'message' => 'Your request was made with invalid credentials.',
                'code' => 0,
                'status' => 401,
            ];
        }

        $user->setPassword($body['new_password']);

        if ($user->save()) {
            Yii::$app->response->statusCode = 200;

            return $this->asJson([
                'message' => 'Password changed successful',
            ]);
        }

        return $user->errors;
    }

    /**
     * {@inheritdoc}
     */
    protected function verbs()
    {
        $verbs = parent::verbs();
        $verbs['index'] = ['post', 'options'];

        return $verbs;
    }
}
