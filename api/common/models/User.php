<?php

namespace api\common\models;

use common\models\Profile;
use Exception;
use OutOfBoundsException;
use Yii;
use yii\db\AfterSaveEvent;

class User extends \common\models\User
{
    public $name;
    public $profile_picture;
    public $provincia;
    public $phone_number;
    public $movil_number;

    public function rules()
    {
        return array_merge([
            [['name', 'profile_picture', 'provincia', 'phone_number', 'movil_number'], 'string'],
        ], parent::rules());
    }

    public static function findByUsername($username)
    {
        return static::findOne(['username' => $username]);
    }

    public function validatePassword($password)
    {
        return Yii::$app->security->validatePassword($password, $this->password_hash);
    }

    /**
     * {@inheritdoc}
     *
     * @param \Lcobucci\JWT\Token $token
     */
    public static function findIdentityByAccessToken($token, $type = null)
    {
        try {
            $uid = $token->getClaim('uid');
        } catch (OutOfBoundsException $e) {
            return null;
        }

        $user = User::findIdentity($uid);

        if (!isset($user)) {
            return null;
        }

        return new static($user);
    }

    public function setPassword($password)
    {
        $this->password_hash = Yii::$app->security->generatePasswordHash($password);
    }

    public function beforeSave($insert)
    {
        if (!$insert) {
            if ($this->name) {
                $this->profile->name = $this->name;
            }

            if ($this->profile_picture) {
                $imagePath = 'profile_pictures/'.Yii::$app->security->generateRandomString();

                $image64 = $this->profile_picture;

                $fileSaved = $this->base64ToImage($image64, $imagePath);

                $this->profile->profile_picture = $fileSaved;
            }

            if ($this->provincia) {
                $this->profile->location = $this->provincia;
            }

            if ($this->phone_number) {
                $this->profile->phone = $this->phone_number;
            }

            if ($this->movil_number) {
                $this->profile->mobile = $this->movil_number;
            }

            $this->profile->save();
        }

        return parent::beforeSave($insert);
    }

    public function afterSave($insert, $changedAttributes)
    {
        if ($insert) {
            $profile = $this->make(Profile::class);

            if ($this->name) {
                $profile->name = $this->name;
            }

            if ($this->profile_picture) {
                $imagePath = 'profile_pictures/'.Yii::$app->security->generateRandomString();

                $image64 = $this->profile_picture;

                $fileSaved = $this->base64ToImage($image64, $imagePath);

                $profile->profile_picture = $fileSaved;
            }

            if ($this->provincia) {
                $profile->location = $this->provincia;
            }

            if ($this->phone_number) {
                $profile->phone = $this->phone_number;
            }

            if ($this->movil_number) {
                $profile->mobile = $this->movil_number;
            }

            $profile->link('user', $this);

            $this->trigger($insert ? self::EVENT_AFTER_INSERT : self::EVENT_AFTER_UPDATE, new AfterSaveEvent([
                'changedAttributes' => $changedAttributes,
            ]));
        } else {
            parent::afterSave($insert, $changedAttributes);
        }
    }

    private static function base64ToImage($base64_string, $output_file)
    {
        try {
            if (
            preg_match(
                '/^data:image\\/(?<extension>(?:png|gif|jpg|jpeg));base64,(?<image>.+)$/',
                $base64_string,
                $matchings
            )) {
                $imageData = base64_decode($matchings['image']);
                $extension = $matchings['extension'];
                $output_file = sprintf('%s.%s', $output_file, $extension);

                $file = fopen($output_file, 'wb');
                fwrite($file, $imageData);
                fclose($file);

                return $output_file;
            }

            // If no match then is an URL (PATCH ;-)
            return $base64_string;
            /*
            if (
            preg_match(
                '/^http.*\\/profile_pictures\\/(?<file>.+)$/',
                $base64_string,
                $matchings
            )) {
                return 'profile_pictures/'.$matchings['file'];
            }
            */
        } catch (Exception $err) {
            throw $err;
        }
    }
}
