<?php

Yii::$container->set(\yii\grid\SerialColumn::class, [
    'options' => ['width' => '50px'],
]);

Yii::$container->set(\yii\grid\ActionColumn::class, [
    'contentOptions' => ['class' => 'text-center'],
    'options' => ['width' => '100px'],
]);
