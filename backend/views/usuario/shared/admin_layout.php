<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use yii\helpers\Html;

/**
 * @var $content string
 */

?>

<?= $this->render(
    '/shared/_alert',
    [
        'module' => Yii::$app->getModule('user'),
    ]
) ?>

<div class="row">
    <div class="col-md-12">
        <div class="card">
            <h3 class="card-header h5"><?= Html::encode($this->title) ?></h3>
            <div class="card-body">
                <?= $this->render('/shared/_menu') ?>
                <?= $content ?>
            </div>
        </div>
    </div>
</div>
