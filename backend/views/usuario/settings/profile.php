<?php

/*
 * This file is part of the 2amigos/yii2-usuario project.
 *
 * (c) 2amigOS! <http://2amigos.us/>
 *
 * For the full copyright and license information, please view
 * the LICENSE file that was distributed with this source code.
 */

use Da\User\Helper\TimezoneHelper;
use yii\helpers\ArrayHelper;
use yii\helpers\Html;
use yii\widgets\ActiveForm;

/**
 * @var yii\web\View $this
 * @var yii\widgets\ActiveForm $form
 * @var \Da\User\Model\Profile $model
 * @var TimezoneHelper $timezoneHelper
 */

$this->title = Yii::t('usuario', 'Profile settings');
$this->params['breadcrumbs'][] = $this->title;
$timezoneHelper = $model->make(TimezoneHelper::class);
?>

<div class="clearfix"></div>

<?= $this->render('/shared/_alert', ['module' => Yii::$app->getModule('user')]) ?>

<div class="row">
    <div class="col-md-3">
        <?= $this->render('_menu') ?>
    </div>
    <div class="col-md-9">
        <?php $form = ActiveForm::begin(
            [
                'id' => $model->formName(),
                'options' => ['class' => 'card'],
                'enableAjaxValidation' => true,
                'enableClientValidation' => false,
                'validateOnBlur' => false,
            ]
        ); ?>

        <h3 class="card-header h5"><?= Html::encode($this->title) ?></h3>
        <div class="card-body">

            <?= $form->field($model, 'name') ?>

            <?= $form->field($model, 'public_email') ?>

            <?= $form->field($model, 'location') ?>

            <div class="row">
                <div class="col-md-6">
                    <?= $form->field($model, 'phone') ?>
                </div>
                <div class="col-md-6">
                    <?= $form->field($model, 'mobile') ?>
                </div>
            </div>

            <?= $form->field($model, 'is_provider')->checkbox() ?>

            <?= $form->field($model, 'bio')->textarea() ?>

        </div>
        <div class="card-footer text-right">
            <?= Html::submitButton(Yii::t('usuario', 'Save'), ['class' => 'btn btn-success']) ?>
        </div>

        <?php ActiveForm::end(); ?>
    </div>
</div>
</div>
