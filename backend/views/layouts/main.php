<?php

/* @var $this \yii\web\View */

/* @var $content string */

use backend\assets\AppAsset;
use yii\helpers\Html;
use yii\bootstrap4\Nav;
use yii\bootstrap4\NavBar;
use yii\bootstrap4\Breadcrumbs;
use common\widgets\Alert;

AppAsset::register($this);
?>
<?php $this->beginPage() ?>
<!DOCTYPE html>
<html lang="<?= Yii::$app->language ?>">
<head>
    <meta charset="<?= Yii::$app->charset ?>">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
    <?php $this->registerCsrfMetaTags() ?>
    <title><?= Html::encode($this->title) ?></title>
    <?php $this->head() ?>
</head>
<body>
<?php $this->beginBody() ?>

<div class="wrap">
    <?php
    NavBar::begin([
        'brandLabel' => Yii::$app->name,
        'brandUrl' => Yii::$app->homeUrl,
        'options' => [
            'class' => 'navbar navbar-expand-md navbar-light bg-light fixed-top',
        ],
    ]);
    /*
    $menuItems = [
        ['label' => Yii::t('app', 'Home'), 'url' => Yii::$app->defaultRoute],
    ];
    */
    if (Yii::$app->user->isGuest) {
        $menuItems[] = ['label' => Yii::t('app', 'Login'), 'url' => ['/user/security/login']];
    } else {
        $menuItems[] = [
            'label' => Yii::t('app', 'Posts'),
            'url' => ['/post/index'],
        ];
        $menuItems[] = [
            'label' => Yii::t('app', 'Feedback'),
            'url' => ['/feedback/index'],
        ];
        $menuItems[] = [
            'label' => Yii::t('app', 'Users'),
            'url' => ['/user/admin'],
            'visible' => Yii::$app->user->can('admin'),
        ];
        $menuItems[] = [
            'label' => Yii::$app->user->identity->username,
            'url' => '#',
            'items' => [
                [
                    'label' => Yii::t('app', 'Profile'),
                    'url' => '/user/settings/profile',
                ],
                [
                    'label' => Yii::t('app', 'Account'),
                    'url' => '/user/settings/account',
                ],
                [
                    'label' => Yii::t('app', "Logout"),
                    'url' => '/user/security/logout',
                    'linkOptions' => [
                        'data-method' => 'post',
                    ]
                ]
            ]
        ];
    }
    echo Nav::widget([
        'options' => ['class' => 'navbar-nav ml-auto'],
        'items' => $menuItems,
    ]);
    NavBar::end();
    ?>

    <div class="container-fluid">
        <?= Breadcrumbs::widget([
            'links' => isset($this->params['breadcrumbs']) ? $this->params['breadcrumbs'] : [],
        ]) ?>
        <?= Alert::widget() ?>
        <?= $content ?>
    </div>
</div>

<footer class="footer">
    <div class="container">
        <p class="pull-left">&copy; <?= Html::encode(Yii::$app->name) ?> <?= date('Y') ?></p>

        <p class="pull-right"><?= Yii::powered() ?></p>
    </div>
</footer>

<?php $this->endBody() ?>
</body>
</html>
<?php $this->endPage() ?>
