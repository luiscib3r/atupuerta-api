<?php

/* @var $this yii\web\View */

$this->title = 'ATuPuerta';
?>
<div class="site-index">

    <div class="jumbotron mt-5">
        <h1 class="mb-5">ATuPuerta Dashboard</h1><br><br><br>
        <?php
        if(Yii::$app->user->isGuest)
         echo '<p><a class="btn btn-lg btn-success" href="/user/security/login">Login</a></p>';
        ?>
    </div>
</div>